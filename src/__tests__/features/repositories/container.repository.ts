import {BelongsToAccessor, Getter, juggler} from '@loopback/repository';
import {DeepCrudRepository} from '../../../repositories';
import {User} from '../models/user.model';
import {Container, ContainerRelations} from './../models/container.model';
import {UserRepository} from './user.repository';

export class ContainerRepository extends DeepCrudRepository<
  Container,
  typeof Container.prototype.id,
  ContainerRelations
> {
  public readonly user: BelongsToAccessor<User, typeof Container.prototype.id>;

  constructor(
    dataSource: juggler.DataSource,
    userRepositoryGetter: Getter<UserRepository>,
  ) {
    super(Container, dataSource);

    this.user = this.createBelongsToAccessorFor('user', userRepositoryGetter);
    this.registerInclusionResolver('user', this.user.inclusionResolver);

    // const userMeta = this.entityClass.definition.relations['user'];
    // this.user = createBelongsToAccessor(
    //     userMeta as BelongsToDefinition,
    //     userRepositoryGetter,
    //     this,
    // );
  }
}
