import {Getter, HasManyRepositoryFactory, juggler} from '@loopback/repository';
import {DeepCrudRepository} from './../../../repositories/deep.crud.repository.base';
import {Blog} from './../models/blog.model';
import {Container} from './../models/container.model';
import {User, UserRelations} from './../models/user.model';
import {BlogRepository} from './blog.repository';
import {ContainerRepository} from './container.repository';

export class UserRepository extends DeepCrudRepository<
  User,
  typeof User.prototype.id,
  UserRelations
> {
  public readonly blogs: HasManyRepositoryFactory<
    Blog,
    typeof User.prototype.id
  >;
  public readonly containers: HasManyRepositoryFactory<
    Container,
    typeof User.prototype.id
  >;

  constructor(
    dataSource: juggler.DataSource,
    protected blogRepositoryGetter: Getter<BlogRepository>,
    protected containerRepositoryGetter: Getter<ContainerRepository>,
  ) {
    super(User, dataSource);

    this.blogs = this.createHasManyRepositoryFactoryFor(
      'blogs',
      blogRepositoryGetter,
    );
    this.registerInclusionResolver('blogs', this.blogs.inclusionResolver);

    // const blogsMeta = this.entityClass.definition.relations['blogs'];
    // this.blogs = createHasManyRepositoryFactory(
    //     blogsMeta as HasManyDefinition,
    //     blogRepositoryGetter
    // );

    this.containers = this.createHasManyRepositoryFactoryFor(
      'containers',
      containerRepositoryGetter,
    );
    this.registerInclusionResolver(
      'containers',
      this.containers.inclusionResolver,
    );

    // const containersMeta = this.entityClass.definition.relations['containers'];
    // this.containers = createHasManyRepositoryFactory(
    //     containersMeta as HasManyDefinition,
    //     containerRepositoryGetter,
    // );
  }
}
