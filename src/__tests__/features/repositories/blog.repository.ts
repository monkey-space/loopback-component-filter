import {BelongsToAccessor, Getter, juggler} from '@loopback/repository';
import {Blog, BlogRelations} from '../models/blog.model';
import {DeepCrudRepository} from './../../../repositories/deep.crud.repository.base';
import {User} from './../models/user.model';
import {UserRepository} from './user.repository';

export class BlogRepository extends DeepCrudRepository<
  Blog,
  typeof Blog.prototype.id,
  BlogRelations
> {
  public readonly user: BelongsToAccessor<User, typeof Blog.prototype.id>;

  constructor(
    dataSource: juggler.DataSource,
    protected userRepositoryGetter: Getter<UserRepository>,
  ) {
    super(Blog, dataSource);

    this.user = this.createBelongsToAccessorFor('user', userRepositoryGetter);
    this.registerInclusionResolver('user', this.user.inclusionResolver);

    // const userMeta = this.entityClass.definition.relations['user'];
    // this.user = createBelongsToAccessor(
    //     userMeta as BelongsToDefinition,
    //     userRepositoryGetter,
    //     this,
    // );
  }
}
