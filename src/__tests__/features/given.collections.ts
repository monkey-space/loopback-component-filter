export const whereWithPredicate = {
  attribute: 'modelo',
  'payments.name': 'example',
  'payments.description': 'example',
};
export const whereWithPredicateInclude = {
  attribute: 'modelo',
  payments: 'null',
  'payments.name': 'example',
  'payments.description': 'example',
};
export const whereSimple = {
  attribute1: '1',
  attribute2: '2',
};
export const whereWithAnd = {
  and: [{prop1: '1', prop2: '2'}],
};
export const whereWithOr = {
  or: [{prop1: '1', prop2: '2'}],
};

export const filterWithIncludesArrayString = {
  where: whereWithPredicate,
  include: ['payments'],
};

export const filterWithIncludesArrayStringTwo = {
  where: whereWithPredicateInclude,
  include: ['payments'],
};

export const filterWithIncludesArrayObject = {
  where: whereWithPredicate,
  include: [{relation: 'payments'}],
};
