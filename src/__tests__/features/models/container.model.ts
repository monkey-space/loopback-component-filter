import {belongsTo, Entity, model, property} from '@loopback/repository';
import {User} from './user.model';

@model()
export class Container extends Entity {
  @property({
    type: 'string',
    defaultFn: 'uuidv4',
    id: true,
  })
  id: string;

  @property({
    type: 'string',
  })
  path: string;

  @property({
    type: 'string',
  })
  lastUpdated: string;

  @belongsTo(() => User)
  userId: string;

  constructor(data?: Partial<Container>) {
    super(data);
  }
}

export interface ContainerRelations {
  // describe navigational properties here
}

export type ContainerWithRelations = Container & ContainerRelations;
