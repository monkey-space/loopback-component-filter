import {Entity, hasMany, model, property} from '@loopback/repository';
import {Blog} from './blog.model';
import {Container} from './container.model';

@model()
export class User extends Entity {
  @property({
    type: 'string',
    defaultFn: 'uuidv4',
    id: true,
  })
  id: string;

  @property({
    type: 'string',
  })
  username: string;

  @property({
    type: 'string',
    unique: true,
  })
  email: string;

  @property({
    type: 'string',
  })
  password: string;

  @hasMany(() => Blog)
  blogs?: Blog[];

  @hasMany(() => Container)
  containers?: Container[];
  constructor(data?: Partial<User>) {
    super(data);
  }
}

export interface UserRelations {
  // describe navigational properties here
}

export type UserWithRelations = User & UserRelations;
