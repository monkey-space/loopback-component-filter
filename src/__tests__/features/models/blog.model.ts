import {belongsTo, Entity, model, property} from '@loopback/repository';
import {User} from './user.model';

@model()
export class Blog extends Entity {
  @property({
    type: 'string',
    defaultFn: 'uuidv4',
    id: true,
  })
  id: string;

  @property({
    type: 'string',
  })
  title: string;

  @property({
    type: 'string',
  })
  content: string;

  @belongsTo(() => User)
  userId: string;

  constructor(data?: Partial<Blog>) {
    super(data);
  }
}

export interface BlogRelations {
  // describe navigational properties here
}

export type BlogWithRelations = Blog & BlogRelations;
