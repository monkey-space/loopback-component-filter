export const givenFilter = [
  {
    id: '06bb9c90-d5c7-4223-9fb2-c5c4b1406b2f',
    username: 'Simon Hackett',
    email: 'Jayne31@yahoo.com',
    password: '2MTcysnmp2bKYmt',
    blogs: [
      {
        id: '3354a590-e058-4293-9f11-8d71afc99db5',
        title: 'Tasty Metal Keyboard',
        content:
          'New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016',
        userId: '06bb9c90-d5c7-4223-9fb2-c5c4b1406b2f',
      },
      {
        id: '471c2e04-c12c-481e-b14a-ca8a61a0a057',
        title: 'Handmade Granite Bike',
        content:
          'The Apollotech B340 is an affordable wireless mouse with reliable connectivity, 12 months battery life and modern design',
        userId: '06bb9c90-d5c7-4223-9fb2-c5c4b1406b2f',
      },
      {
        id: 'b63164ea-4e8f-4121-b5cb-eceb2063cf01',
        title: 'Small Granite Chair',
        content:
          'The Nagasaki Lander is the trademarked name of several series of Nagasaki sport bikes, that started with the 1984 ABC800J',
        userId: '06bb9c90-d5c7-4223-9fb2-c5c4b1406b2f',
      },
    ],
    containers: [
      {
        id: '742eed19-8cc2-4561-bdd4-75b1aef4f149',
        path: '/usr/obj',
        lastUpdated: '2021-08-19T14:42:23.691Z',
        userId: '06bb9c90-d5c7-4223-9fb2-c5c4b1406b2f',
      },
    ],
  },
  {
    id: '8f5464ab-bd6c-48db-aebc-2ad155ae5653',
    username: 'Paula Gorczany III',
    email: 'Albertha.Huel@gmail.com',
    password: 'plUoqwgOA5Sg_Ln',
    blogs: [
      {
        id: '04a4a41d-0f61-4843-83cd-ee9a1d54428a',
        title: 'Licensed Frozen Cheese',
        content:
          'Carbonite web goalkeeper gloves are ergonomically designed to give easy fit',
        userId: '8f5464ab-bd6c-48db-aebc-2ad155ae5653',
      },
      {
        id: '66a127da-b7a6-4d10-9325-fe0abbd68937',
        title: 'Awesome Soft Fish',
        content:
          'New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart',
        userId: '8f5464ab-bd6c-48db-aebc-2ad155ae5653',
      },
      {
        id: 'a0fdd6b0-6c9f-4650-8ec3-38b71ae9ad1b',
        title: 'Handmade Granite Sausages',
        content:
          'Carbonite web goalkeeper gloves are ergonomically designed to give easy fit',
        userId: '8f5464ab-bd6c-48db-aebc-2ad155ae5653',
      },
      {
        id: 'e078e44b-00db-430b-a9a2-dd3bb82cefe8',
        title: 'Awesome Soft Sausages',
        content:
          'The beautiful range of Apple Naturalé that has an exciting mix of natural ingredients. With the Goodness of 100% Natural Ingredients',
        userId: '8f5464ab-bd6c-48db-aebc-2ad155ae5653',
      },
    ],
    containers: [
      {
        id: '9258867d-7eed-416d-9c69-3f80c39f994e',
        path: '/usr/obj',
        lastUpdated: '2021-08-19T17:40:24.640Z',
        userId: '8f5464ab-bd6c-48db-aebc-2ad155ae5653',
      },
      {
        id: 'f9512b42-008d-4667-8304-3c60b818b565',
        path: '/usr/obj',
        lastUpdated: '2021-08-19T19:44:14.426Z',
        userId: '8f5464ab-bd6c-48db-aebc-2ad155ae5653',
      },
    ],
  },
];
