export const givenBlog = [
  {
    title: 'Small Granite Chair',
    content:
      'The Nagasaki Lander is the trademarked name of several series of Nagasaki sport bikes, that started with the 1984 ABC800J',
    userId: '06bb9c90-d5c7-4223-9fb2-c5c4b1406b2f',
    id: 'b63164ea-4e8f-4121-b5cb-eceb2063cf01',
  },
  {
    title: 'Handmade Granite Bike',
    content:
      'The Apollotech B340 is an affordable wireless mouse with reliable connectivity, 12 months battery life and modern design',
    userId: '06bb9c90-d5c7-4223-9fb2-c5c4b1406b2f',
    id: '471c2e04-c12c-481e-b14a-ca8a61a0a057',
  },
  {
    title: 'Tasty Metal Keyboard',
    content:
      'New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016',
    userId: '06bb9c90-d5c7-4223-9fb2-c5c4b1406b2f',
    id: '3354a590-e058-4293-9f11-8d71afc99db5',
  },
  {
    title: 'Tasty Wooden Car',
    content:
      'The automobile layout consists of a front-engine design, with transaxle-type transmissions mounted at the rear of the engine and four wheel drive',
    userId: '60dbc93e-e8a0-4fb4-be47-3e46370d4fc9',
    id: 'b50629c9-036c-4c53-873e-b9ce2dfb300c',
  },
  {
    title: 'Handmade Granite Pants',
    content:
      'New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016',
    userId: '60dbc93e-e8a0-4fb4-be47-3e46370d4fc9',
    id: '8d21d5ad-360f-4fc3-bffc-b1cd762b641f',
  },
  {
    title: 'Sleek Wooden Towels',
    content:
      'New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart',
    userId: '60dbc93e-e8a0-4fb4-be47-3e46370d4fc9',
    id: '143dc6cd-7e1e-4994-845b-9ff59bc613b9',
  },
  {
    title: 'Unbranded Steel Shoes',
    content: 'The Football Is Good For Training And Recreational Purposes',
    userId: '60dbc93e-e8a0-4fb4-be47-3e46370d4fc9',
    id: '6d56356c-bea5-4442-b497-33527ee40022',
  },
  {
    title: 'Licensed Steel Hat',
    content:
      'New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart',
    userId: '60dbc93e-e8a0-4fb4-be47-3e46370d4fc9',
    id: '48f9145c-14d6-410b-b86a-e94a9ed6e645',
  },
  {
    title: 'Rustic Metal Gloves',
    content:
      'The slim & simple Maple Gaming Keyboard from Dev Byte comes with a sleek body and 7- Color RGB LED Back-lighting for smart functionality',
    userId: '25dd68fb-565d-431e-9fea-57abcde5484c',
    id: '09da1c11-fd4e-4b45-a8af-b43e29fd6cca',
  },
  {
    title: 'Ergonomic Metal Hat',
    content:
      'New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016',
    userId: '25dd68fb-565d-431e-9fea-57abcde5484c',
    id: '10806864-383f-432c-a29d-2f15a71870b7',
  },
  {
    title: 'Unbranded Granite Shoes',
    content:
      "Boston's most advanced compression wear technology increases muscle oxygenation, stabilizes active muscles",
    userId: '25dd68fb-565d-431e-9fea-57abcde5484c',
    id: '073195c6-4f7e-43b9-8e23-ddface307a43',
  },
  {
    title: 'Handmade Granite Sausages',
    content:
      'Carbonite web goalkeeper gloves are ergonomically designed to give easy fit',
    userId: '0dd284b8-c196-4bd5-a8d6-1eecb850f478',
    id: 'c38855c5-3208-4c9f-ae28-77474336b2cb',
  },
  {
    title: 'Handcrafted Frozen Chicken',
    content:
      'Andy shoes are designed to keeping in mind durability as well as trends, the most stylish range of shoes & sandals',
    userId: '0dd284b8-c196-4bd5-a8d6-1eecb850f478',
    id: 'a95070ce-f64e-48d2-98f4-df18e116916d',
  },
  {
    title: 'Generic Granite Car',
    content:
      'The Nagasaki Lander is the trademarked name of several series of Nagasaki sport bikes, that started with the 1984 ABC800J',
    userId: '0dd284b8-c196-4bd5-a8d6-1eecb850f478',
    id: '812c2add-502c-4931-9f89-ac9fe4856459',
  },
  {
    title: 'Small Steel Pants',
    content:
      'New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016',
    userId: '0dd284b8-c196-4bd5-a8d6-1eecb850f478',
    id: '9d43bd9b-9ea4-4cc3-b66d-c420ea68e20d',
  },
  {
    title: 'Awesome Soft Sausages',
    content:
      'The beautiful range of Apple Naturalé that has an exciting mix of natural ingredients. With the Goodness of 100% Natural Ingredients',
    userId: '8f5464ab-bd6c-48db-aebc-2ad155ae5653',
    id: 'e078e44b-00db-430b-a9a2-dd3bb82cefe8',
  },
  {
    title: 'Licensed Frozen Cheese',
    content:
      'Carbonite web goalkeeper gloves are ergonomically designed to give easy fit',
    userId: '8f5464ab-bd6c-48db-aebc-2ad155ae5653',
    id: '04a4a41d-0f61-4843-83cd-ee9a1d54428a',
  },
  {
    title: 'Handmade Granite Sausages',
    content:
      'Carbonite web goalkeeper gloves are ergonomically designed to give easy fit',
    userId: '8f5464ab-bd6c-48db-aebc-2ad155ae5653',
    id: 'a0fdd6b0-6c9f-4650-8ec3-38b71ae9ad1b',
  },
  {
    title: 'Awesome Soft Fish',
    content:
      'New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart',
    userId: '8f5464ab-bd6c-48db-aebc-2ad155ae5653',
    id: '66a127da-b7a6-4d10-9325-fe0abbd68937',
  },
  {
    title: 'Fantastic Concrete Sausages',
    content:
      'New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart',
    userId: '61dc6585-5e43-4786-8e6b-78a20a8c698c',
    id: '20262c42-a87f-42bc-a8e6-65225bf7887c',
  },
  {
    title: 'Unbranded Soft Chips',
    content:
      'The automobile layout consists of a front-engine design, with transaxle-type transmissions mounted at the rear of the engine and four wheel drive',
    userId: '61dc6585-5e43-4786-8e6b-78a20a8c698c',
    id: '828e1695-f6fe-43e6-b811-84df087cba84',
  },
  {
    title: 'Handcrafted Plastic Car',
    content:
      'New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016',
    userId: '61dc6585-5e43-4786-8e6b-78a20a8c698c',
    id: '55472103-9e35-4763-8502-f32dc17be7b4',
  },
  {
    title: 'Handcrafted Cotton Chicken',
    content:
      'New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016',
    userId: '61dc6585-5e43-4786-8e6b-78a20a8c698c',
    id: 'a66b0904-dc4f-4e40-b87f-deb304d36342',
  },
  {
    title: 'Sleek Soft Sausages',
    content:
      'New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016',
    userId: 'f5f7ccb8-bc1d-4727-abc4-ef01a9affb85',
    id: '014c6dd0-4146-4ff4-9dc3-470e7160c8f5',
  },
  {
    title: 'Incredible Rubber Bacon',
    content:
      'Carbonite web goalkeeper gloves are ergonomically designed to give easy fit',
    userId: 'f5f7ccb8-bc1d-4727-abc4-ef01a9affb85',
    id: '45a99b26-7c5e-46f8-a8e7-23a40c0ccc89',
  },
  {
    title: 'Refined Soft Ball',
    content:
      'The beautiful range of Apple Naturalé that has an exciting mix of natural ingredients. With the Goodness of 100% Natural Ingredients',
    userId: 'f5f7ccb8-bc1d-4727-abc4-ef01a9affb85',
    id: '387a645c-8bb8-4094-89d2-56b6107f3ed0',
  },
  {
    title: 'Intelligent Steel Pants',
    content: 'The Football Is Good For Training And Recreational Purposes',
    userId: 'f5f7ccb8-bc1d-4727-abc4-ef01a9affb85',
    id: '74376afc-c99f-4b42-a353-0e8b308d5cf2',
  },
];
