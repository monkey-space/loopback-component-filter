/* eslint-disable prefer-const */
import {juggler} from '@loopback/repository';
import {givenBlog, givenContainer} from '../features/fakes';
import {givenUser} from './../features/fakes/user.fake';
import {BlogRepository} from './../features/repositories/blog.repository';
import {ContainerRepository} from './../features/repositories/container.repository';
import {UserRepository} from './../features/repositories/user.repository';

export const dataSource = new juggler.DataSource({
  name: 'db',
  connector: 'memory',
  file: './db.json',
});

export async function initialize() {
  let userRepo: UserRepository;
  let blogRepo: BlogRepository;
  let containerRepo: ContainerRepository;

  userRepo = new UserRepository(
    dataSource,
    async () => blogRepo,
    async () => containerRepo,
  );
  const userTable = await userRepo.count();
  if (userTable.count === 0) await userRepo.createAll(givenUser);

  blogRepo = new BlogRepository(dataSource, async () => userRepo);
  const blogTable = await blogRepo.count();
  if (blogTable.count === 0) await blogRepo.createAll(givenBlog);

  containerRepo = new ContainerRepository(dataSource, async () => userRepo);
  const containerTable = await containerRepo.count();
  if (containerTable.count === 0) await containerRepo.createAll(givenContainer);

  return {userRepo, blogRepo, containerRepo};
}
