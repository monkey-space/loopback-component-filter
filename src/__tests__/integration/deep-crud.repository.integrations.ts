import {AnyObject} from '@loopback/repository';
import {expect} from '@loopback/testlab';
import {givenUser} from '../features/fakes/user.fake';
import {givenResponseFilter} from '../features/given.response.filter';
import {UserRepository} from '../features/repositories/user.repository';
import {givenFilter} from './../features/given.filter';
import {initialize} from './datasource.fake';

describe('Deep Crud Repository Base Test', () => {
  let userRepo: UserRepository;

  before(async () => {
    ({userRepo} = await initialize());
  });

  it('find() Test', async () => {
    const users = await userRepo.find();
    expect(users).containDeep(givenUser);
  });

  it('find() Test with deep filter', async () => {
    const filter: AnyObject = {
      where: {
        containers: 'undefined',
        'containers.path': '/usr/obj',
        //blogs: "undefined",
      },
      include: ['containers', 'blogs'],
    };
    const usersWithRelations = await userRepo.find(filter);

    expect(usersWithRelations).is.not.undefined();
    expect(usersWithRelations).to.containDeep(givenResponseFilter);
  });

  it('find() Test with deep filter where containers.path', async () => {
    const filter: AnyObject = {
      where: {
        //containers: "undefined",
        'containers.path': '/usr/obj',
        //blogs: "undefined",
      },
      include: ['containers', 'blogs'],
    };

    const usersWithRelations = await userRepo.find(filter);

    expect(usersWithRelations).is.not.undefined();
    expect(usersWithRelations).is.not.empty();
    expect(usersWithRelations).to.containDeepOrdered(givenFilter);
  });
});
