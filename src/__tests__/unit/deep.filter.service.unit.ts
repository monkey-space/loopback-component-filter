import {expect} from '@loopback/testlab';
import {DeepFilterService} from './../../services/deep-filter.service';
import {
  filterWithIncludesArrayObject,
  filterWithIncludesArrayString,
  filterWithIncludesArrayStringTwo,
  whereSimple,
  whereWithAnd,
  whereWithOr,
  whereWithPredicate,
  whereWithPredicateInclude
} from './../features/given.collections';

// import { User } from "./test.model";
// import { UserRepository } from "./test.repository";

describe('Deep Filter Service', () => {
  const deepFilterService: DeepFilterService = new DeepFilterService();

  it('return string if object where is validate', () => {
    const result = deepFilterService.isFormat(whereWithPredicate);
    expect(result).is.String();
  });

  it('return string if object where is validate: case one', () => {
    const result = deepFilterService.isFormat(whereWithPredicateInclude);
    console.log(result);
    expect(result).is.String();
  });

  it('return undefined if object where is not valid', () => {
    const result = deepFilterService.isFormat(whereSimple);
    expect(result).is.undefined();
  });

  it('return error if object where have condition and', () => {
    expect.throws(() => {
      deepFilterService.isFormat(whereWithAnd);
    }, 'No support to condition and | or');
  });

  it('return error if object where have condition or', () => {
    expect.throws(() => {
      deepFilterService.isFormat(whereWithOr);
    }, 'No support to condition and | or');
  });

  it('return object conditional filterWithIncludesArrayString to filter `getObjectConditionByRelation`', () => {
    const expectedResult = {
      'payments.name': 'example',
      'payments.description': 'example',
    };
    const [attributes, properties] =
      deepFilterService.getObjectConditionByRelation(
        filterWithIncludesArrayString,
      );

    expect(attributes).is.empty();
    expect(properties).to.Object().containEql(expectedResult);
  });

  it('return object conditional filterWithIncludesArrayStringTwo to filter `getObjectConditionByRelation`', () => {
    const expectedResult = {
      'payments.name': 'example',
      'payments.description': 'example',
    };
    const [attributes, properties] =
      deepFilterService.getObjectConditionByRelation(
        filterWithIncludesArrayStringTwo,
      );

    expect(attributes).to.Object().containEql({payments: 'null'});
    expect(properties).to.Object().containEql(expectedResult);
  });

  it('return object conditional filterWithIncludesArrayObject to filter `getObjectConditionByRelation`', () => {
    const expectedResult = {
      'payments.name': 'example',
      'payments.description': 'example',
    };
    const [attributes, properties] =
      deepFilterService.getObjectConditionByRelation(
        filterWithIncludesArrayObject,
      );

    expect(attributes).is.empty();
    expect(properties).to.Object().containEql(expectedResult);
  });

  it('return empty object conditional whereSimple to filter `getObjectConditionByRelation`', () => {
    const [attributes, properties] =
      deepFilterService.getObjectConditionByRelation({
        where: whereSimple,
      });

    expect(attributes).is.Undefined();
    expect(properties).is.Undefined();
  });

  it('return array to relations from filter include', () => {
    const expectedResult: string[] = ['payments'];
    const {include} = filterWithIncludesArrayObject;
    const relations = deepFilterService.getRelationsValue(include);

    expect(relations).to.eql(expectedResult);
  });

  it('return format string to conditional from properties result filter where', () => {
    const [attributes, properties] =
      deepFilterService.getObjectConditionByRelation(
        filterWithIncludesArrayObject,
      );
    const expectedResult =
      '"payments.name" === "example" && "payments.description" === "example"';
    const format = deepFilterService.convertConditionalBy(properties);

    expect(attributes).is.empty();
    expect(format).to.equal(expectedResult);
  });

  it('return format string to conditional from 2 attributes result filter where', () => {
    const attributes = {
      containers: 'undefined',
      blogs: 'undefined',
    };
    const expectedResult =
      '"containers" === "undefined" && "blogs" === "undefined"';
    const format = deepFilterService.convertConditionalBy(attributes);

    expect(format).to.equal(expectedResult);
  });

  it('return format string to conditional using relation from properties result filter where', () => {
    const [attributes, properties] =
      deepFilterService.getObjectConditionByRelation(
        filterWithIncludesArrayObject,
      );
    const expectedResult =
      '"payments.name" === "example" && "payments.description" === "example"';
    const format = deepFilterService.convertConditionalBy(
      properties,
      'payments',
    );

    expect(attributes).is.empty();
    expect(format).to.equal(expectedResult);
  });

  it('return object where without format using filterWithIncludesArrayStringTwo', () => {
    const result = deepFilterService.getWhereWithoutFormat(
      filterWithIncludesArrayStringTwo,
    );
    const expectedResult = {
      attribute: 'modelo',
    };

    expect(result).to.containEql(expectedResult);
  });

  it('return object where without format using filterWithIncludesArrayString', () => {
    const result = deepFilterService.getWhereWithoutFormat(
      filterWithIncludesArrayString,
    );
    const expectedResult = {
      attribute: 'modelo',
    };

    expect(result).to.containEql(expectedResult);
  });

  it('return object where', () => {
    const xon = [
      {
        "created": "2021-07-29T23:43:56.000Z",
        "lastUpdated": "2021-07-29T23:43:56.000Z",
        "id": "0e2c8e92-d5d7-11eb-b8bc-0242ac130003",
        "status": "ACTIVE",
        "description": null,
        "suscriptors": 1,
        "market": "BINANCE",
        "avgRating": 3,
        "numRatings": 2,
        "timeFrame": 15,
        "symbolId": "a57dfcc2-9be4-11eb-a8b3-0242ac130003",
        "creatorId": "30cbe4f6-0fce-4d35-a047-887b5c18609e",
        "ratings": [
          {
            "created": "2021-08-07T23:11:55.000Z",
            "lastUpdated": "2021-08-07T23:11:55.000Z",
            "value": 3,
            "id": "81bab475-6805-4e18-b449-eb023d8be794",
            "description": null,
            "userId": "603990ec-da29-4526-9fbc-6ce19af62731",
            "trackingRoomId": "0e2c8e92-d5d7-11eb-b8bc-0242ac130003"
          }
        ],
        "symbol": {
          "id": "a57dfcc2-9be4-11eb-a8b3-0242ac130003",
          "name": "Bitcoin / TetherUS",
          "abbreviation": "BTCUSDT",
          "icons": [
            "https://storage.googleapis.com/signal-buckets/symbols/btc.svg",
            "https://storage.googleapis.com/signal-buckets/symbols/usdt.svg"
          ],
          "description": null,
          "symbolCategoryId": "41cd4bd6-bc91-45e1-ae6b-bbf45f0351c6"
        },
        "creator": {
          "created": "2021-07-29T23:43:58.000Z",
          "lastUpdated": "2021-07-29T23:43:58.000Z",
          "id": "30cbe4f6-0fce-4d35-a047-887b5c18609e",
          "name": "Shirley Lazarte",
          "email": "senalesalerta@gmail.com",
          "password": null,
          "points": 88,
          "pushToken": null,
          "language": "ES",
          "socketId": null,
          "enableEmail": null,
          "googleCode": null,
          "confirmation": false,
          "roleId": "305c3502-8a7a-49e9-9d97-81b5baf18882"
        }
      },
      {
        "created": "2021-07-29T23:43:56.000Z",
        "lastUpdated": "2021-07-29T23:43:56.000Z",
        "id": "129d75fd-c10e-4422-b802-23b2cb9bb0db",
        "status": "ACTIVE",
        "description": null,
        "suscriptors": 1,
        "market": "BINANCE",
        "avgRating": 5,
        "numRatings": 3,
        "timeFrame": 15,
        "symbolId": "2cf3f8be-9be5-11eb-a8b3-0242ac130003",
        "creatorId": "30cbe4f6-0fce-4d35-a047-887b5c18609e",
        "ratings": [
          {
            "created": "2021-08-07T13:05:09.000Z",
            "lastUpdated": "2021-08-07T13:05:09.000Z",
            "value": 5,
            "id": "26ddee4e-6cd5-4335-81be-bc389a2d4aa8",
            "description": null,
            "userId": "603990ec-da29-4526-9fbc-6ce19af62731",
            "trackingRoomId": "129d75fd-c10e-4422-b802-23b2cb9bb0db"
          },
          {
            "created": "2021-08-21T18:20:41.000Z",
            "lastUpdated": "2021-08-21T18:20:41.000Z",
            "value": 4,
            "id": "b3d8597c-925b-4db4-88f0-24d4f8bcaea9",
            "description": null,
            "userId": "679451f5-a074-40d1-80ff-57683674315f",
            "trackingRoomId": "129d75fd-c10e-4422-b802-23b2cb9bb0db"
          }
        ],
        "symbol": {
          "id": "2cf3f8be-9be5-11eb-a8b3-0242ac130003",
          "name": "Binance Coin / TetherUS",
          "abbreviation": "BNBUSDT",
          "icons": [
            "https://storage.googleapis.com/signal-buckets/symbols/bnb.svg",
            "https://storage.googleapis.com/signal-buckets/symbols/usdt.svg"
          ],
          "description": null,
          "symbolCategoryId": "41cd4bd6-bc91-45e1-ae6b-bbf45f0351c6"
        },
        "creator": {
          "created": "2021-07-29T23:43:58.000Z",
          "lastUpdated": "2021-07-29T23:43:58.000Z",
          "id": "30cbe4f6-0fce-4d35-a047-887b5c18609e",
          "name": "Shirley Lazarte",
          "email": "senalesalerta@gmail.com",
          "password": null,
          "points": 88,
          "pushToken": null,
          "language": "ES",
          "socketId": null,
          "enableEmail": null,
          "googleCode": null,
          "confirmation": false,
          "roleId": "305c3502-8a7a-49e9-9d97-81b5baf18882"
        }
      },
      {
        "created": "2021-08-02T20:36:59.000Z",
        "lastUpdated": "2021-08-02T20:36:59.000Z",
        "id": "2a6d98bb-448d-4a22-95f7-844624d531dd",
        "status": "ACTIVE",
        "description": null,
        "suscriptors": 0,
        "market": "BINANCE",
        "avgRating": 3,
        "numRatings": 2,
        "timeFrame": 15,
        "symbolId": "0c100720-5765-4937-9e45-4a075362f6d8",
        "creatorId": "603990ec-da29-4526-9fbc-6ce19af62731",
        "ratings": [
          {
            "created": "2021-08-07T13:21:42.000Z",
            "lastUpdated": "2021-08-07T13:21:42.000Z",
            "value": 3,
            "id": "271b2a35-55c7-4568-8cf3-d25fa64d2d34",
            "description": null,
            "userId": "603990ec-da29-4526-9fbc-6ce19af62731",
            "trackingRoomId": "2a6d98bb-448d-4a22-95f7-844624d531dd"
          }
        ],
        "symbol": {
          "id": "0c100720-5765-4937-9e45-4a075362f6d8",
          "name": "Bitcoin / TetherUS PERPETUAL FUTURES",
          "abbreviation": "BTCUSDTPERP",
          "icons": [
            "https://storage.googleapis.com/signal-buckets/symbols/btc.svg",
            "https://storage.googleapis.com/signal-buckets/symbols/usdtperp.svg"
          ],
          "description": null,
          "symbolCategoryId": "41cd4bd6-bc91-45e1-ae6b-bbf45f0351c6"
        },
        "creator": {
          "created": "2021-07-30T00:03:58.000Z",
          "lastUpdated": "2021-08-21T20:14:14.000Z",
          "id": "603990ec-da29-4526-9fbc-6ce19af62731",
          "name": "Fernando Mamani P.",
          "email": "gg.bug.ty.izi@gmail.com",
          "password": "false",
          "points": 4486,
          "pushToken": "dv1m4gy0SZCST0aNRs2mVT:APA91bFJssIeNnkgWKJ2kpqqcvHCxZd9GwohrpNtrcOxec0LdqmYK_OEQy_cPBNicQkJcZQmbtYsNaxkKpeZ-ya2ca0WOApTjWsE7zV1oaaq7sUIuEl_gRcBkduuqS_hzxvsN3qmGEBJ",
          "language": "ES",
          "socketId": "S4UBXErGbTPcfKP4AAQr",
          "enableEmail": false,
          "googleCode": "114807078378958752403",
          "confirmation": true,
          "roleId": "305c3502-8a7a-49e9-9d97-81b5baf18882"
        }
      },
      {
        "created": "2021-08-05T23:23:18.000Z",
        "lastUpdated": "2021-08-05T23:23:18.000Z",
        "id": "4ae836b3-644f-4b4e-97b6-28ec3b8e6cab",
        "status": "ACTIVE",
        "description": "",
        "suscriptors": -1,
        "market": "BYBIT",
        "avgRating": 4,
        "numRatings": 5,
        "timeFrame": 15,
        "symbolId": "20f7c11c-9be5-11eb-a8b3-0242ac130003",
        "creatorId": "603990ec-da29-4526-9fbc-6ce19af62731",
        "ratings": [
          {
            "created": "2021-08-18T15:47:29.000Z",
            "lastUpdated": "2021-08-18T15:47:29.000Z",
            "value": 5,
            "id": "40fd864c-c938-46ec-8024-5d2591c72705",
            "description": null,
            "userId": "7db5c9de-f357-4e27-a5fb-2941ea85e02b",
            "trackingRoomId": "4ae836b3-644f-4b4e-97b6-28ec3b8e6cab"
          },
          {
            "created": "2021-08-10T01:50:12.000Z",
            "lastUpdated": "2021-08-10T01:50:12.000Z",
            "value": 3,
            "id": "518b40af-96b3-48aa-87dd-8ce602c95ac7",
            "description": null,
            "userId": "755d4779-353a-4958-8bf8-c68fe63a7de4",
            "trackingRoomId": "4ae836b3-644f-4b4e-97b6-28ec3b8e6cab"
          },
          {
            "created": "2021-08-07T23:15:30.000Z",
            "lastUpdated": "2021-08-07T23:15:30.000Z",
            "value": 5,
            "id": "9b38405f-38e7-45db-96b3-11d3ef8ccd6f",
            "description": null,
            "userId": "603990ec-da29-4526-9fbc-6ce19af62731",
            "trackingRoomId": "4ae836b3-644f-4b4e-97b6-28ec3b8e6cab"
          },
          {
            "created": "2021-08-21T03:33:11.000Z",
            "lastUpdated": "2021-08-21T03:33:11.000Z",
            "value": 3,
            "id": "c441b0d0-c986-4702-8453-628881f4814a",
            "description": null,
            "userId": "679451f5-a074-40d1-80ff-57683674315f",
            "trackingRoomId": "4ae836b3-644f-4b4e-97b6-28ec3b8e6cab"
          }
        ],
        "symbol": {
          "id": "20f7c11c-9be5-11eb-a8b3-0242ac130003",
          "name": "Bitcoin / U.S. dollar",
          "abbreviation": "BTCUSD",
          "icons": [
            "https://storage.googleapis.com/signal-buckets/symbols/btc.svg",
            "https://storage.googleapis.com/signal-buckets/symbols/usd.svg"
          ],
          "description": null,
          "symbolCategoryId": "41cd4bd6-bc91-45e1-ae6b-bbf45f0351c6"
        },
        "creator": {
          "created": "2021-07-30T00:03:58.000Z",
          "lastUpdated": "2021-08-21T20:14:14.000Z",
          "id": "603990ec-da29-4526-9fbc-6ce19af62731",
          "name": "Fernando Mamani P.",
          "email": "gg.bug.ty.izi@gmail.com",
          "password": "false",
          "points": 4486,
          "pushToken": "dv1m4gy0SZCST0aNRs2mVT:APA91bFJssIeNnkgWKJ2kpqqcvHCxZd9GwohrpNtrcOxec0LdqmYK_OEQy_cPBNicQkJcZQmbtYsNaxkKpeZ-ya2ca0WOApTjWsE7zV1oaaq7sUIuEl_gRcBkduuqS_hzxvsN3qmGEBJ",
          "language": "ES",
          "socketId": "S4UBXErGbTPcfKP4AAQr",
          "enableEmail": false,
          "googleCode": "114807078378958752403",
          "confirmation": true,
          "roleId": "305c3502-8a7a-49e9-9d97-81b5baf18882"
        }
      },
      {
        "created": "2021-08-05T23:22:35.000Z",
        "lastUpdated": "2021-08-05T23:22:35.000Z",
        "id": "7310b9b3-9e00-4f77-9b3b-cc93e358403f",
        "status": "ACTIVE",
        "description": "",
        "suscriptors": 2,
        "market": "HITBTC",
        "avgRating": 4,
        "numRatings": 4,
        "timeFrame": 15,
        "symbolId": "2cf3f8be-9be5-11eb-a8b3-0242ac130003",
        "creatorId": "603990ec-da29-4526-9fbc-6ce19af62731",
        "ratings": [
          {
            "created": "2021-08-21T04:19:07.000Z",
            "lastUpdated": "2021-08-21T04:19:07.000Z",
            "value": 3,
            "id": "19b2edb5-1829-4c8e-85e7-8ac6db18d462",
            "description": null,
            "userId": "679451f5-a074-40d1-80ff-57683674315f",
            "trackingRoomId": "7310b9b3-9e00-4f77-9b3b-cc93e358403f"
          },
          {
            "created": "2021-08-07T23:56:09.000Z",
            "lastUpdated": "2021-08-07T23:56:09.000Z",
            "value": 3,
            "id": "5f8a94c7-c41e-4adb-addd-a2758ed55ad2",
            "description": null,
            "userId": "603990ec-da29-4526-9fbc-6ce19af62731",
            "trackingRoomId": "7310b9b3-9e00-4f77-9b3b-cc93e358403f"
          },
          {
            "created": "2021-08-18T01:49:50.000Z",
            "lastUpdated": "2021-08-18T01:49:50.000Z",
            "value": 5,
            "id": "75020a35-7936-4714-b201-8f077b404d97",
            "description": null,
            "userId": "7db5c9de-f357-4e27-a5fb-2941ea85e02b",
            "trackingRoomId": "7310b9b3-9e00-4f77-9b3b-cc93e358403f"
          }
        ],
        "symbol": {
          "id": "2cf3f8be-9be5-11eb-a8b3-0242ac130003",
          "name": "Binance Coin / TetherUS",
          "abbreviation": "BNBUSDT",
          "icons": [
            "https://storage.googleapis.com/signal-buckets/symbols/bnb.svg",
            "https://storage.googleapis.com/signal-buckets/symbols/usdt.svg"
          ],
          "description": null,
          "symbolCategoryId": "41cd4bd6-bc91-45e1-ae6b-bbf45f0351c6"
        },
        "creator": {
          "created": "2021-07-30T00:03:58.000Z",
          "lastUpdated": "2021-08-21T20:14:14.000Z",
          "id": "603990ec-da29-4526-9fbc-6ce19af62731",
          "name": "Fernando Mamani P.",
          "email": "gg.bug.ty.izi@gmail.com",
          "password": "false",
          "points": 4486,
          "pushToken": "dv1m4gy0SZCST0aNRs2mVT:APA91bFJssIeNnkgWKJ2kpqqcvHCxZd9GwohrpNtrcOxec0LdqmYK_OEQy_cPBNicQkJcZQmbtYsNaxkKpeZ-ya2ca0WOApTjWsE7zV1oaaq7sUIuEl_gRcBkduuqS_hzxvsN3qmGEBJ",
          "language": "ES",
          "socketId": "S4UBXErGbTPcfKP4AAQr",
          "enableEmail": false,
          "googleCode": "114807078378958752403",
          "confirmation": true,
          "roleId": "305c3502-8a7a-49e9-9d97-81b5baf18882"
        }
      },
      {
        "created": "2021-08-05T01:36:03.000Z",
        "lastUpdated": "2021-08-12T14:24:54.000Z",
        "id": "937cd257-b058-4663-ac5e-0e52e4c5efe4",
        "status": "ACTIVE",
        "description": "",
        "suscriptors": 0,
        "market": "OANDA",
        "avgRating": 5,
        "numRatings": 2,
        "timeFrame": 15,
        "symbolId": "1b57b98f-582b-4344-9c11-7a393b8b06ca",
        "creatorId": "603990ec-da29-4526-9fbc-6ce19af62731",
        "ratings": [
          {
            "created": "2021-08-18T15:48:18.000Z",
            "lastUpdated": "2021-08-18T15:48:18.000Z",
            "value": 5,
            "id": "8c37f99a-08c1-4336-9fc0-4040c2af7008",
            "description": null,
            "userId": "7db5c9de-f357-4e27-a5fb-2941ea85e02b",
            "trackingRoomId": "937cd257-b058-4663-ac5e-0e52e4c5efe4"
          },
          {
            "created": "2021-08-21T04:21:11.000Z",
            "lastUpdated": "2021-08-21T04:21:11.000Z",
            "value": 5,
            "id": "bf0b1b4b-fbf5-4fbf-9c9d-c432742483b3",
            "description": null,
            "userId": "679451f5-a074-40d1-80ff-57683674315f",
            "trackingRoomId": "937cd257-b058-4663-ac5e-0e52e4c5efe4"
          }
        ],
        "symbol": {
          "id": "1b57b98f-582b-4344-9c11-7a393b8b06ca",
          "name": "U.S. Dollar/Japanese Yen",
          "abbreviation": "USDJPY",
          "icons": [
            "https://storage.googleapis.com/signal-buckets/symbols/usd.svg",
            "https://storage.googleapis.com/signal-buckets/symbols/jpy.svg"
          ],
          "description": null,
          "symbolCategoryId": "23efa56b-86c8-4ea0-b267-a435b3b6fe1e"
        },
        "creator": {
          "created": "2021-07-30T00:03:58.000Z",
          "lastUpdated": "2021-08-21T20:14:14.000Z",
          "id": "603990ec-da29-4526-9fbc-6ce19af62731",
          "name": "Fernando Mamani P.",
          "email": "gg.bug.ty.izi@gmail.com",
          "password": "false",
          "points": 4486,
          "pushToken": "dv1m4gy0SZCST0aNRs2mVT:APA91bFJssIeNnkgWKJ2kpqqcvHCxZd9GwohrpNtrcOxec0LdqmYK_OEQy_cPBNicQkJcZQmbtYsNaxkKpeZ-ya2ca0WOApTjWsE7zV1oaaq7sUIuEl_gRcBkduuqS_hzxvsN3qmGEBJ",
          "language": "ES",
          "socketId": "S4UBXErGbTPcfKP4AAQr",
          "enableEmail": false,
          "googleCode": "114807078378958752403",
          "confirmation": true,
          "roleId": "305c3502-8a7a-49e9-9d97-81b5baf18882"
        }
      },
      {
        "created": "2021-08-02T20:03:22.000Z",
        "lastUpdated": "2021-08-02T20:03:22.000Z",
        "id": "957f3f6d-eb07-418e-b9e1-7f781560fd23",
        "status": "ACTIVE",
        "description": null,
        "suscriptors": 2,
        "market": "BINANCE",
        "avgRating": 0,
        "numRatings": 0,
        "timeFrame": 15,
        "symbolId": "0c100720-5765-4937-9e45-4a075362f6d8",
        "creatorId": "603990ec-da29-4526-9fbc-6ce19af62731",
        "symbol": {
          "id": "0c100720-5765-4937-9e45-4a075362f6d8",
          "name": "Bitcoin / TetherUS PERPETUAL FUTURES",
          "abbreviation": "BTCUSDTPERP",
          "icons": [
            "https://storage.googleapis.com/signal-buckets/symbols/btc.svg",
            "https://storage.googleapis.com/signal-buckets/symbols/usdtperp.svg"
          ],
          "description": null,
          "symbolCategoryId": "41cd4bd6-bc91-45e1-ae6b-bbf45f0351c6"
        },
        "creator": {
          "created": "2021-07-30T00:03:58.000Z",
          "lastUpdated": "2021-08-21T20:14:14.000Z",
          "id": "603990ec-da29-4526-9fbc-6ce19af62731",
          "name": "Fernando Mamani P.",
          "email": "gg.bug.ty.izi@gmail.com",
          "password": "false",
          "points": 4486,
          "pushToken": "dv1m4gy0SZCST0aNRs2mVT:APA91bFJssIeNnkgWKJ2kpqqcvHCxZd9GwohrpNtrcOxec0LdqmYK_OEQy_cPBNicQkJcZQmbtYsNaxkKpeZ-ya2ca0WOApTjWsE7zV1oaaq7sUIuEl_gRcBkduuqS_hzxvsN3qmGEBJ",
          "language": "ES",
          "socketId": "S4UBXErGbTPcfKP4AAQr",
          "enableEmail": false,
          "googleCode": "114807078378958752403",
          "confirmation": true,
          "roleId": "305c3502-8a7a-49e9-9d97-81b5baf18882"
        }
      },
      {
        "created": "2021-07-29T23:43:56.000Z",
        "lastUpdated": "2021-07-29T23:43:56.000Z",
        "id": "97a7a076-cac0-4590-895e-962d4c0e808b",
        "status": "ACTIVE",
        "description": null,
        "suscriptors": -3,
        "market": "BINANCE",
        "avgRating": 2,
        "numRatings": 7,
        "timeFrame": 15,
        "symbolId": "a57dfcc2-9be4-11eb-a8b3-0242ac130003",
        "creatorId": "30cbe4f6-0fce-4d35-a047-887b5c18609e",
        "ratings": [
          {
            "created": "2021-08-21T18:11:53.000Z",
            "lastUpdated": "2021-08-21T18:11:53.000Z",
            "value": 4,
            "id": "364d49af-a94c-4324-8ff2-67c7e5638582",
            "description": null,
            "userId": "679451f5-a074-40d1-80ff-57683674315f",
            "trackingRoomId": "97a7a076-cac0-4590-895e-962d4c0e808b"
          },
          {
            "created": "2021-08-07T13:04:26.000Z",
            "lastUpdated": "2021-08-07T13:04:26.000Z",
            "value": 0,
            "id": "f89119c6-1015-40df-9402-880888739035",
            "description": null,
            "userId": "603990ec-da29-4526-9fbc-6ce19af62731",
            "trackingRoomId": "97a7a076-cac0-4590-895e-962d4c0e808b"
          }
        ],
        "symbol": {
          "id": "a57dfcc2-9be4-11eb-a8b3-0242ac130003",
          "name": "Bitcoin / TetherUS",
          "abbreviation": "BTCUSDT",
          "icons": [
            "https://storage.googleapis.com/signal-buckets/symbols/btc.svg",
            "https://storage.googleapis.com/signal-buckets/symbols/usdt.svg"
          ],
          "description": null,
          "symbolCategoryId": "41cd4bd6-bc91-45e1-ae6b-bbf45f0351c6"
        },
        "creator": {
          "created": "2021-07-29T23:43:58.000Z",
          "lastUpdated": "2021-07-29T23:43:58.000Z",
          "id": "30cbe4f6-0fce-4d35-a047-887b5c18609e",
          "name": "Shirley Lazarte",
          "email": "senalesalerta@gmail.com",
          "password": null,
          "points": 88,
          "pushToken": null,
          "language": "ES",
          "socketId": null,
          "enableEmail": null,
          "googleCode": null,
          "confirmation": false,
          "roleId": "305c3502-8a7a-49e9-9d97-81b5baf18882"
        }
      },
      {
        "created": "2021-08-04T23:46:21.000Z",
        "lastUpdated": "2021-08-12T14:30:55.000Z",
        "id": "a7495e2b-95fd-4ee3-9bb6-fb7ed8b48b1b",
        "status": "ACTIVE",
        "description": "",
        "suscriptors": 0,
        "market": "OANDA",
        "avgRating": 3,
        "numRatings": 3,
        "timeFrame": 15,
        "symbolId": "1b57b98f-582b-4344-9c11-7a393b8b06ca",
        "creatorId": "603990ec-da29-4526-9fbc-6ce19af62731",
        "ratings": [
          {
            "created": "2021-08-07T23:11:11.000Z",
            "lastUpdated": "2021-08-07T23:11:11.000Z",
            "value": 3,
            "id": "0d9c0ff2-fde5-4550-a76f-b750783db16b",
            "description": null,
            "userId": "603990ec-da29-4526-9fbc-6ce19af62731",
            "trackingRoomId": "a7495e2b-95fd-4ee3-9bb6-fb7ed8b48b1b"
          },
          {
            "created": "2021-08-21T18:24:09.000Z",
            "lastUpdated": "2021-08-21T18:24:09.000Z",
            "value": 3,
            "id": "2dba26ab-5ae9-4907-b414-34daaf7b4a25",
            "description": null,
            "userId": "679451f5-a074-40d1-80ff-57683674315f",
            "trackingRoomId": "a7495e2b-95fd-4ee3-9bb6-fb7ed8b48b1b"
          }
        ],
        "symbol": {
          "id": "1b57b98f-582b-4344-9c11-7a393b8b06ca",
          "name": "U.S. Dollar/Japanese Yen",
          "abbreviation": "USDJPY",
          "icons": [
            "https://storage.googleapis.com/signal-buckets/symbols/usd.svg",
            "https://storage.googleapis.com/signal-buckets/symbols/jpy.svg"
          ],
          "description": null,
          "symbolCategoryId": "23efa56b-86c8-4ea0-b267-a435b3b6fe1e"
        },
        "creator": {
          "created": "2021-07-30T00:03:58.000Z",
          "lastUpdated": "2021-08-21T20:14:14.000Z",
          "id": "603990ec-da29-4526-9fbc-6ce19af62731",
          "name": "Fernando Mamani P.",
          "email": "gg.bug.ty.izi@gmail.com",
          "password": "false",
          "points": 4486,
          "pushToken": "dv1m4gy0SZCST0aNRs2mVT:APA91bFJssIeNnkgWKJ2kpqqcvHCxZd9GwohrpNtrcOxec0LdqmYK_OEQy_cPBNicQkJcZQmbtYsNaxkKpeZ-ya2ca0WOApTjWsE7zV1oaaq7sUIuEl_gRcBkduuqS_hzxvsN3qmGEBJ",
          "language": "ES",
          "socketId": "S4UBXErGbTPcfKP4AAQr",
          "enableEmail": false,
          "googleCode": "114807078378958752403",
          "confirmation": true,
          "roleId": "305c3502-8a7a-49e9-9d97-81b5baf18882"
        }
      },
      {
        "created": "2021-08-04T20:53:40.000Z",
        "lastUpdated": "2021-08-04T20:53:40.000Z",
        "id": "b2dc02d0-66b7-443b-89e2-701d861b7595",
        "status": "ACTIVE",
        "description": "",
        "suscriptors": 0,
        "market": "OKEX",
        "avgRating": 5,
        "numRatings": 1,
        "timeFrame": 15,
        "symbolId": "0c100720-5765-4937-9e45-4a075362f6d8",
        "creatorId": "603990ec-da29-4526-9fbc-6ce19af62731",
        "ratings": [
          {
            "created": "2021-08-10T01:51:04.000Z",
            "lastUpdated": "2021-08-10T01:51:04.000Z",
            "value": 5,
            "id": "b1e4609c-306b-4564-8345-b1ff240d8ad1",
            "description": null,
            "userId": "755d4779-353a-4958-8bf8-c68fe63a7de4",
            "trackingRoomId": "b2dc02d0-66b7-443b-89e2-701d861b7595"
          }
        ],
        "symbol": {
          "id": "0c100720-5765-4937-9e45-4a075362f6d8",
          "name": "Bitcoin / TetherUS PERPETUAL FUTURES",
          "abbreviation": "BTCUSDTPERP",
          "icons": [
            "https://storage.googleapis.com/signal-buckets/symbols/btc.svg",
            "https://storage.googleapis.com/signal-buckets/symbols/usdtperp.svg"
          ],
          "description": null,
          "symbolCategoryId": "41cd4bd6-bc91-45e1-ae6b-bbf45f0351c6"
        },
        "creator": {
          "created": "2021-07-30T00:03:58.000Z",
          "lastUpdated": "2021-08-21T20:14:14.000Z",
          "id": "603990ec-da29-4526-9fbc-6ce19af62731",
          "name": "Fernando Mamani P.",
          "email": "gg.bug.ty.izi@gmail.com",
          "password": "false",
          "points": 4486,
          "pushToken": "dv1m4gy0SZCST0aNRs2mVT:APA91bFJssIeNnkgWKJ2kpqqcvHCxZd9GwohrpNtrcOxec0LdqmYK_OEQy_cPBNicQkJcZQmbtYsNaxkKpeZ-ya2ca0WOApTjWsE7zV1oaaq7sUIuEl_gRcBkduuqS_hzxvsN3qmGEBJ",
          "language": "ES",
          "socketId": "S4UBXErGbTPcfKP4AAQr",
          "enableEmail": false,
          "googleCode": "114807078378958752403",
          "confirmation": true,
          "roleId": "305c3502-8a7a-49e9-9d97-81b5baf18882"
        }
      },
      {
        "created": "2021-08-05T00:16:25.000Z",
        "lastUpdated": "2021-08-05T00:16:25.000Z",
        "id": "d4c14e42-91d8-4339-b7c8-2df6d391a875",
        "status": "ACTIVE",
        "description": "testest",
        "suscriptors": 0,
        "market": "BYBIT",
        "avgRating": 3,
        "numRatings": 3,
        "timeFrame": 15,
        "symbolId": "20f7c11c-9be5-11eb-a8b3-0242ac130003",
        "creatorId": "603990ec-da29-4526-9fbc-6ce19af62731",
        "ratings": [
          {
            "created": "2021-08-21T18:21:58.000Z",
            "lastUpdated": "2021-08-21T18:21:58.000Z",
            "value": 4,
            "id": "2cbb0f55-5221-4163-9608-de59f898cac9",
            "description": null,
            "userId": "679451f5-a074-40d1-80ff-57683674315f",
            "trackingRoomId": "d4c14e42-91d8-4339-b7c8-2df6d391a875"
          },
          {
            "created": "2021-08-18T15:51:46.000Z",
            "lastUpdated": "2021-08-18T15:51:46.000Z",
            "value": 5,
            "id": "31c74fa4-3edc-4b5c-b8b6-10dadbd5f590",
            "description": null,
            "userId": "7db5c9de-f357-4e27-a5fb-2941ea85e02b",
            "trackingRoomId": "d4c14e42-91d8-4339-b7c8-2df6d391a875"
          },
          {
            "created": "2021-08-10T01:50:30.000Z",
            "lastUpdated": "2021-08-10T01:50:30.000Z",
            "value": 1,
            "id": "49142ebc-b4dc-4994-8bdd-ddda51c3d234",
            "description": null,
            "userId": "755d4779-353a-4958-8bf8-c68fe63a7de4",
            "trackingRoomId": "d4c14e42-91d8-4339-b7c8-2df6d391a875"
          }
        ],
        "symbol": {
          "id": "20f7c11c-9be5-11eb-a8b3-0242ac130003",
          "name": "Bitcoin / U.S. dollar",
          "abbreviation": "BTCUSD",
          "icons": [
            "https://storage.googleapis.com/signal-buckets/symbols/btc.svg",
            "https://storage.googleapis.com/signal-buckets/symbols/usd.svg"
          ],
          "description": null,
          "symbolCategoryId": "41cd4bd6-bc91-45e1-ae6b-bbf45f0351c6"
        },
        "creator": {
          "created": "2021-07-30T00:03:58.000Z",
          "lastUpdated": "2021-08-21T20:14:14.000Z",
          "id": "603990ec-da29-4526-9fbc-6ce19af62731",
          "name": "Fernando Mamani P.",
          "email": "gg.bug.ty.izi@gmail.com",
          "password": "false",
          "points": 4486,
          "pushToken": "dv1m4gy0SZCST0aNRs2mVT:APA91bFJssIeNnkgWKJ2kpqqcvHCxZd9GwohrpNtrcOxec0LdqmYK_OEQy_cPBNicQkJcZQmbtYsNaxkKpeZ-ya2ca0WOApTjWsE7zV1oaaq7sUIuEl_gRcBkduuqS_hzxvsN3qmGEBJ",
          "language": "ES",
          "socketId": "S4UBXErGbTPcfKP4AAQr",
          "enableEmail": false,
          "googleCode": "114807078378958752403",
          "confirmation": true,
          "roleId": "305c3502-8a7a-49e9-9d97-81b5baf18882"
        }
      }
    ];
    const result = deepFilterService.mapDeep(
      xon,
      {
        where: {
          status: "ACTIVE",
          // "creator": "undefined",
          // "creator.email": "senalesalerta@gmail.com",

          "ratings": "undefined",
          "ratings.value": "3"
        },
        include: ['creator', 'symbol', 'ratings']
      }
    );
    console.log(result);

  })
});
