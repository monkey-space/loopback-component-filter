import {BindingKey, CoreBindings} from '@loopback/core';
import {LoopbackFilterComponent} from './component';
import {DeepFilterService} from './services/deep-filter.service';

/**
 * Binding keys used by this component.
 */
export namespace LoopbackFilterComponentBindings {
  export const COMPONENT = BindingKey.create<LoopbackFilterComponent>(
    `${CoreBindings.COMPONENTS}.LoopbackFilterComponent`,
  );

  export const DEEP_FILTER_SERVICE = BindingKey.create<DeepFilterService>(
    'services.deep-filter.service',
  );
}
