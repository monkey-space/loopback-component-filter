import {Entity, EntityCrudRepository, juggler} from '@loopback/repository';
/**
 * Interface defining the component's options object
 */
export interface LoopbackFilterComponentOptions {
  // Add the definitions here
}

/**
 * Default options for the component
 */
export const DEFAULT_LOOPBACK_COMPONENT_FILTER_OPTIONS: LoopbackFilterComponentOptions =
  {
    // Specify the values here
  };

export type DeepCrudRepositoryCtor = new <
  T extends Entity,
  ID,
  Relations extends object,
>(
  entityClass: typeof Entity & {prototype: T},
  dataSource: juggler.DataSource,
) => EntityCrudRepository<T, ID, Relations>;
