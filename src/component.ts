import {
  Application,
  Binding,
  Component,
  config,
  ContextTags,
  CoreBindings,
  inject,
  injectable,
} from '@loopback/core';
import {LoopbackFilterComponentBindings} from './keys';
import {DeepFilterService} from './services/deep-filter.service';
import {
  DEFAULT_LOOPBACK_COMPONENT_FILTER_OPTIONS,
  LoopbackFilterComponentOptions,
} from './types';

// Configure the binding for LoopbackFilterComponent
@injectable({
  tags: {[ContextTags.KEY]: LoopbackFilterComponentBindings.COMPONENT},
})
export class LoopbackFilterComponent implements Component {
  bindings: Binding[] = [
    Binding.bind(LoopbackFilterComponentBindings.DEEP_FILTER_SERVICE).toClass(
      DeepFilterService,
    ),
  ];

  constructor(
    @inject(CoreBindings.APPLICATION_INSTANCE)
    private application: Application,
    @config()
    private options: LoopbackFilterComponentOptions = DEFAULT_LOOPBACK_COMPONENT_FILTER_OPTIONS,
  ) {}
}
